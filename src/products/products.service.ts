/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const product = new Product();
    product.name = createProductDto.name;
    product.price = parseFloat(createProductDto.price);
    product.type = JSON.parse(createProductDto.type);
    if (createProductDto.image && createProductDto.image !== '') {
      product.image = createProductDto.image;
    }
    return this.productsRepository.save(product);
  }

  findAll() {
    return this.productsRepository.find({ relations: { type: true } });
  }

  findAllByType(typeId: number) {
    return this.productsRepository.find({
      where: { typeId: typeId },
      relations: { type: true },
      order: { name: 'ASC' },
    });
  }
  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updateProduct = await this.productsRepository.findOne({
      where: { id },
    });
    updateProduct.name = updateProductDto.name;
    updateProduct.price = parseFloat(updateProductDto.price);
    updateProduct.type = JSON.parse(updateProductDto.type);
    if (updateProductDto.image && updateProductDto.image !== '') {
      updateProduct.image = updateProductDto.image;
    }
    this.productsRepository.save(updateProduct);

    const result = await this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneByOrFail({ id });
    await this.productsRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
