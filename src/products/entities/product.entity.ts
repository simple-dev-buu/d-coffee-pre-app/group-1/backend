/* eslint-disable prettier/prettier */
import { OrderItem } from 'src/orders/entities/orderItem.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { Type } from 'src/types/entities/type.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  typeId: number;

  @ManyToOne(() => Type, (type) => type.products)
  type: Type;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];

  @ManyToMany(() => Promotion, (promotion) => promotion.products)
  @JoinTable()
  promotions: Promotion[];
}

//CRUD PRODUCT
