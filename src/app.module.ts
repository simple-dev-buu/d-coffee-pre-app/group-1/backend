import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PromotionModule } from './promotion/promotion.module';
import { ProductsModule } from './products/products.module';
import { Stock } from './stock/entities/stock.entity';
import { StockModule } from './stock/stock.module';
import { DataSource } from 'typeorm';
import { OrdersModule } from './orders/orders.module';
import { TypesModule } from './types/types.module';
import { BranchesModule } from './branches/branches.module';
import { CustomersModule } from './customers/customers.module';
import { BillsModule } from './bills/bills.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { InventoryModule } from './Inventory/inventory.module';
import { StockTakingModule } from './StockTaking/stockTaking.module';
import { ReplenishmentModule } from './Replenishment/replenishment.module';
import { InventoryItem } from './Inventory/entities/inventoryItems.entity';
import { StockTakingItem } from './StockTaking/entities/stockTakingItem.entity';
import { Ingredient } from './ingredient/entities/ingredient.entity';
import { Replenishment } from './Replenishment/entities/replenishment.entity';
import { ReplenishmentItem } from './Replenishment/entities/replenishmentItem.entity';
import { User } from './users/entities/user.entity';
import { IngredientModule } from './ingredient/ingredient.module';
import { Role } from './roles/entities/role.entity';
import { Order } from './orders/entities/order.entity';
import { RolesModule } from './roles/roles.module';
import { EmployeesModule } from './employees/employees.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'd-coffee.sqlite',
      autoLoadEntities: true,
      synchronize: true,
      entities: [
        InventoryItem,
        StockTakingItem,
        Ingredient,
        Replenishment,
        ReplenishmentItem,
        Stock,
        User,
        Role,
        Order,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    PromotionModule,
    ProductsModule,
    StockModule,
    OrdersModule,
    TypesModule,
    BranchesModule,
    CustomersModule,
    BillsModule,
    AuthModule,
    UsersModule,
    InventoryModule,
    StockTakingModule,
    ReplenishmentModule,
    IngredientModule,
    AuthModule,
    RolesModule,
    EmployeesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
