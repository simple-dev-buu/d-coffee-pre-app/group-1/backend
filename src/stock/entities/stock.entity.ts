import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  code: string;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  balance: number;

  @Column()
  unit: number;

  @Column()
  status: 'L' | 'M';

  // @ManyToOne(() => Branch, (branch) => branch.stock)
  // branch: Branch;
}
