export class CreateStockDto {
  code: string;
  name: string;
  price: number;
  balance: number;
  unit: number;
  status: 'L' | 'M';
}
