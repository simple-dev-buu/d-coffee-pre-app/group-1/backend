import { Injectable } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    const customer = new Customer();
    customer.name = createCustomerDto.name;
    customer.telephone = createCustomerDto.telephone;
    customer.birthDay = createCustomerDto.birthDay;
    customer.point = createCustomerDto.point;
    return this.customerRepository.save(customer);
  }

  findAll() {
    return this.customerRepository.find();
  }

  findOne(id: number) {
    return this.customerRepository.findOneByOrFail({ id });
  }

  findOneByTelephone(telephone: string) {
    return this.customerRepository.findOneByOrFail({ telephone: telephone });
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    await this.customerRepository.findOneByOrFail({ id });
    await this.customerRepository.update(id, updateCustomerDto);
    const updatedType = await this.customerRepository.findOneBy({ id });
    return updatedType;
  }

  async remove(id: number) {
    const deletedCustomer = await this.customerRepository.findOneByOrFail({
      id,
    });
    await this.customerRepository.remove(deletedCustomer);
    return deletedCustomer;
  }
}
