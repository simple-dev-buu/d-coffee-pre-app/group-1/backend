export class CreateCustomerDto {
  name: string;
  telephone: string;
  birthDay: Date;
  point: number;
  status: boolean;
}
