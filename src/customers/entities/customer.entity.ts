import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  telephone: string;

  @Column()
  birthDay: Date;

  @Column({ default: 0 })
  point: number;

  @CreateDateColumn()
  registerDate: Date;

  @UpdateDateColumn()
  lastestOrderDate: Date;

  @Column({ default: true })
  status: boolean;

  @OneToMany(() => Order, (order) => order.customer)
  order: Order[];
}
