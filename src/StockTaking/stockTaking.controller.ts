import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { StockTakingService } from './stockTaking.service';
import { CreateStockTakingDto } from './dto/create-stockTaking.dto';
import { UpdateStockTakingDto } from './dto/update-stockTaking.dto';

@Controller('StockTaking')
export class StockTakingController {
  constructor(private readonly StockTakingService: StockTakingService) {}

  @Post()
  create(@Body() createStockTakingDto: CreateStockTakingDto) {
    return this.StockTakingService.create(createStockTakingDto);
  }

  @Get()
  findAll() {
    return this.StockTakingService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.StockTakingService.findOne(+id);
  }

  @Patch()
  update(@Body() id: number, updateStockTakingDto: UpdateStockTakingDto) {
    return this.StockTakingService.update(+id, updateStockTakingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.StockTakingService.remove(+id);
  }
}
