/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateStockTakingDto } from './dto/create-stockTaking.dto';
import { UpdateStockTakingDto } from './dto/update-stockTaking.dto';
import { StockTaking } from './entities/stockTaking.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class StockTakingService {
  constructor(
    @InjectRepository(StockTaking)
    private readonly stockTakingRepository: Repository<StockTaking>,
  ) {}
  create(createStockTakingDto: CreateStockTakingDto) {
    return this.stockTakingRepository.save(createStockTakingDto);
  }

  findAll() {
    return this.stockTakingRepository.find();
  }

  findOne(id: number) {
    return this.stockTakingRepository.findOneBy({ id: id });
  }

  update(id: number, updateStockTakingDto: UpdateStockTakingDto) {
    return this.stockTakingRepository.update(id, updateStockTakingDto);
  }

  async remove(id: number) {
    await this.stockTakingRepository.delete(id);
  }
}
