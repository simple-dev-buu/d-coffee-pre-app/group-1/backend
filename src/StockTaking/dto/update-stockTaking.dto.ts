import { PartialType } from '@nestjs/mapped-types';
import { CreateStockTakingDto } from './create-stockTaking.dto';

export class UpdateStockTakingDto extends PartialType(CreateStockTakingDto) {}
