/* eslint-disable prettier/prettier */
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { StockTakingItem } from './stockTakingItem.entity';
import { Inventory } from 'src/Inventory/entities/inventory.entity';

@Entity()
export class StockTaking {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dateCreated: Date;

  @OneToMany(() => Inventory, (inventory) => inventory.inventoryItem)
  inventory: Inventory[];

  @OneToMany(
    () => StockTakingItem,
    (stockTakingItem) => stockTakingItem.stockTaking,
  )
  stockTakingItem: StockTakingItem[];
}
