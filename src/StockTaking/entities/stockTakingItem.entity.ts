import { Branch } from 'src/branches/entities/branch.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { StockTaking } from './stockTaking.entity';
import { InventoryItem } from 'src/Inventory/entities/inventoryItems.entity';

@Entity()
export class StockTakingItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  balanceUpdate: number;

  @Column()
  balanceOld: number;

  @OneToMany(() => StockTaking, (stockTaking) => stockTaking.stockTakingItem)
  stockTaking: StockTaking[];

  @OneToMany(() => InventoryItem, (inventoryItem) => inventoryItem.inventory)
  inventoryItem: InventoryItem[];
}
