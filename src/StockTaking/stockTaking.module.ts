import { Module } from '@nestjs/common';
import { StockTakingService } from './stockTaking.service';
import { StockTakingController } from './stockTaking.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockTaking } from './entities/stockTaking.entity';
import { Branch } from 'src/branches/entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StockTaking, Branch])],
  controllers: [StockTakingController],
  providers: [StockTakingService],
})
export class StockTakingModule {}
