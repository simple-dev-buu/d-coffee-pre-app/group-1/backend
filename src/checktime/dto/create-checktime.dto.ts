import { User } from 'src/users/entities/user.entity';

export class CreateChecktimeDto {
  date: string;
  user: User | null;
  clockIn: string;
  clockOut: string;
  timeWorked: Date;
  typeWork: 'Missing' | 'Normal' | 'Normal/OT';
}
