import { PartialType } from '@nestjs/mapped-types';
import { CreateChecktimeDto } from './create-checktime.dto';

export class UpdateChecktimeDto extends PartialType(CreateChecktimeDto) {}
