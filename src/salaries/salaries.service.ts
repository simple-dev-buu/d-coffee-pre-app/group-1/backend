import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SalariesService {
  constructor(
    @InjectRepository(Salary)
    private readonly salaryRepo: Repository<Salary>,
  ) {}

  create(createSalaryDto: CreateSalaryDto) {
    return this.salaryRepo.save(createSalaryDto);
  }

  findAll() {
    return this.salaryRepo.find();
  }

  findOne(id: number) {
    return this.salaryRepo.findOneBy({ id });
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    return this.salaryRepo.update(id, updateSalaryDto);
  }

  async remove(id: number) {
    return this.salaryRepo.delete(id);
  }
}
