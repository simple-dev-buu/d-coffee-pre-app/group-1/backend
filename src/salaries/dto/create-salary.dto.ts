export class CreateSalaryDto {
  date: string;
  period: string;
  employeeID: number;
  roles: 'admin' | 'user';
  name: string;
  timeWorked: number;
  defaultSalary: number;
  lossSalary: number;
  netSalary: number;
  state: 'รอชำระ' | 'ชำระแล้ว';
}
