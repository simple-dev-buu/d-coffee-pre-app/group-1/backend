import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  period: string;

  @Column()
  employeeID: number;

  @Column()
  roles: 'admin' | 'user';

  @Column()
  name: string;

  @Column()
  timeWorked: number;

  @Column()
  defaultSalary: number;

  @Column()
  lossSalary: number;

  @Column()
  netSalary: number;

  @Column()
  state: 'รอชำระ' | 'ชำระแล้ว';
}
