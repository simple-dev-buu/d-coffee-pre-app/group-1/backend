import { Inventory } from 'src/Inventory/entities/inventory.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  location: string;

  @Column({ default: true })
  status: boolean;

  @CreateDateColumn()
  createDate: Date;

  @OneToMany(() => Order, (order) => order.branch)
  order: Order[];

  @OneToMany(() => Inventory, (inventory) => inventory.branch)
  inventory: Inventory[];

  @OneToMany(() => Bill, (bill) => bill.branch)
  bill: Bill[];
}
