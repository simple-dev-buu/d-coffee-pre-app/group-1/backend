export class CreateBranchDto {
  name: string;
  location: string;
  status: boolean;
}
