import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Branch } from './entities/branch.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BranchesService {
  constructor(
    @InjectRepository(Branch)
    private readonly branchRepository: Repository<Branch>,
  ) {}

  create(createBranchDto: CreateBranchDto) {
    const branch = new Branch();
    branch.name = createBranchDto.name;
    branch.location = createBranchDto.location;
    return this.branchRepository.save(branch);
  }

  findAll() {
    return this.branchRepository.find();
  }

  findOne(id: number) {
    return this.branchRepository.findOneByOrFail({ id });
  }

  findOneByName(name: string) {
    return this.branchRepository.findOneByOrFail({ name });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    await this.branchRepository.findOneByOrFail({ id });
    await this.branchRepository.update(id, updateBranchDto);
    const updatedType = await this.branchRepository.findOneBy({ id });
    return updatedType;
  }

  async remove(id: number) {
    const removedBranch = await this.branchRepository.findOneByOrFail({ id });
    await this.branchRepository.remove(removedBranch);
    return removedBranch;
  }
}
