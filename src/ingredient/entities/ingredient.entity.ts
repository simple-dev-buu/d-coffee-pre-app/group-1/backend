import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { InventoryItem } from 'src/Inventory/entities/inventoryItems.entity';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  unit: string;

  @OneToMany(() => InventoryItem, (inventoryItem) => inventoryItem.ingredient)
  inventoryItem: InventoryItem[];
}
