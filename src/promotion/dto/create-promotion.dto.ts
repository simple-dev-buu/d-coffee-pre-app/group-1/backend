export class CreatePromotionDto {
  name: string;
  description: string;
  discount: number;
  combo: boolean;
  startDate: Date;
  endDate: Date;
  status: boolean;
}
