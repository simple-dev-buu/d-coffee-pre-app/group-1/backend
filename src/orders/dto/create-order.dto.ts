export class CreateOrderDto {
  orderItems: {
    productId: number;
    name: string;
    price: number;
    unit: number;
  }[];
  userId: number;
  branchId: number;
  customerId: number;
  promotionId: number;
}
