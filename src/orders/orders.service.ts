import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
// import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
import { Branch } from 'src/branches/entities/branch.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(Branch)
    private readonly branchRepository: Repository<Branch>,
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order = new Order();
    const user = await this.usersRepository.findOneBy({
      id: createOrderDto.userId,
    });
    const branch = await this.branchRepository.findOneBy({
      id: createOrderDto.branchId,
    });
    const customer = await this.customerRepository.findOneBy({
      id: createOrderDto.customerId,
    });

    if (customer) {
      order.customer = customer;
    }
    if (branch) {
      order.branch = branch;
    }
    order.user = user;
    order.netPrice = 0;
    order.totalUnit = 0;
    order.orderItems = [];
    for (const oi of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.unit = oi.unit;
      orderItem.total = orderItem.price * orderItem.unit;
      await this.orderItemsRepository.save(orderItem);
      order.orderItems.push(orderItem);
      order.netPrice += orderItem.total;
      order.totalUnit += orderItem.unit;
    }
    if (order.customer) {
      console.log('Have Member');
      order.customer.point = order.customer.point + order.totalUnit;
      console.log('Updated Point' + order.customer.point);
      await this.customerRepository.update(order.customer.id, order.customer);
    }
    console.log(order.branch);
    console.log(order.customer);
    return this.ordersRepository.save(order);
  }

  findAll() {
    return this.ordersRepository.find({
      relations: {
        orderItems: true,
      },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOneOrFail({
      where: { id },
      relations: { orderItems: true },
    });
  }

  // update(id: number, updateOrderDto: UpdateOrderDto) {
  //   return `This action updates a #${id} order`;
  // }

  async remove(id: number) {
    const deleteOrder = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
