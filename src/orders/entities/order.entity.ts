import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './orderItem.entity';
import { Branch } from 'src/branches/entities/branch.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { User } from 'src/users/entities/user.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  totalUnit: number;

  @Column()
  totalBefore: number;

  @Column()
  discount: number;

  @Column()
  netPrice: number;

  @Column()
  receivedAmount: number;

  @Column()
  change: number;

  @Column()
  paymentType: string;

  @Column()
  givePoint: number;

  @Column()
  usePoint: number;

  @Column()
  totalPoint: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  @ManyToOne(() => Branch, (branch) => branch.order)
  branch: Branch;

  @ManyToOne(() => Customer, (customer) => customer.order)
  customer: Customer;

  @ManyToOne(() => User, (user) => user.orders)
  user: User;
  static user: any;

  @ManyToOne(() => Promotion, (promotion) => promotion.orders)
  promotion: Promotion;
}
