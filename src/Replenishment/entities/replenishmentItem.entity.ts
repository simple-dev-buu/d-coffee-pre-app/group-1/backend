import { Branch } from 'src/branches/entities/branch.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Replenishment } from './replenishment.entity';
import { InventoryItem } from 'src/Inventory/entities/inventoryItems.entity';

@Entity()
export class ReplenishmentItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  balanceUpdate: number;

  @Column()
  balanceOld: number;

  @Column()
  netValue: number;

  @OneToMany(
    () => Replenishment,
    (replenishment) => replenishment.replenishmentItem,
  )
  replenishment: Replenishment[];

  @OneToMany(() => InventoryItem, (inventoryItem) => inventoryItem.inventory)
  inventoryItem: InventoryItem[];
}
