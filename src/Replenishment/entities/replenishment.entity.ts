/* eslint-disable prettier/prettier */
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { ReplenishmentItem } from './replenishmentItem.entity';
import { Inventory } from 'src/Inventory/entities/inventory.entity';

@Entity()
export class Replenishment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dateCreated: Date;

  @Column()
  totalNetPrice: number;

  @OneToMany(() => Inventory, (inventory) => inventory.inventoryItem)
  inventory: Inventory[];

  @OneToMany(
    () => ReplenishmentItem,
    (replenishmentItem) => replenishmentItem.replenishment,
  )
  replenishmentItem: ReplenishmentItem[];
}
