/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateReplenishmentDto } from './dto/create-replenishment.dto';
import { UpdateReplenishmentDto } from './dto/update-replenishment.dto';
import { Replenishment } from './entities/replenishment.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ReplenishmentService {
  constructor(
    @InjectRepository(Replenishment)
    private readonly replenishmentRepository: Repository<Replenishment>,
  ) {}
  create(createReplenishmentDto: CreateReplenishmentDto) {
    return this.replenishmentRepository.save(createReplenishmentDto);
  }

  findAll() {
    return this.replenishmentRepository.find();
  }

  findOne(id: number) {
    return this.replenishmentRepository.findOneBy({ id: id });
  }

  update(id: number, updateReplenishmentDto: UpdateReplenishmentDto) {
    return this.replenishmentRepository.update(id, updateReplenishmentDto);
  }

  async remove(id: number) {
    await this.replenishmentRepository.delete(id);
  }
}
