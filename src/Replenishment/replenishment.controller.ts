import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ReplenishmentService } from './replenishment.service';
import { CreateReplenishmentDto } from './dto/create-replenishment.dto';
import { UpdateReplenishmentDto } from './dto/update-replenishment.dto';

@Controller('Replenishment')
export class ReplenishmentController {
  constructor(private readonly ReplenishmentService: ReplenishmentService) {}

  @Post()
  create(@Body() createReplenishmentDto: CreateReplenishmentDto) {
    return this.ReplenishmentService.create(createReplenishmentDto);
  }

  @Get()
  findAll() {
    return this.ReplenishmentService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ReplenishmentService.findOne(+id);
  }

  @Patch()
  update(@Body() id: number, updateReplenishmentDto: UpdateReplenishmentDto) {
    return this.ReplenishmentService.update(+id, updateReplenishmentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ReplenishmentService.remove(+id);
  }
}
