export class CreateReplenishmentDto {
  dateCreated: Date;
  totalNetPrice: number;
}
