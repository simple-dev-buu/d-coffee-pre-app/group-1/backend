import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { InventoryService } from './inventory.service';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { UpdateInventoryDto } from './dto/update-inventory.dto';

@Controller('Inventory')
export class InventoryController {
  constructor(private readonly InventoryService: InventoryService) {}

  @Post()
  create(@Body() createInventoryDto: CreateInventoryDto) {
    return this.InventoryService.create(createInventoryDto);
  }

  @Get()
  findAll() {
    return this.InventoryService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.InventoryService.findOne(+id);
  }

  @Patch()
  update(@Body() id: number, updateInventoryDto: UpdateInventoryDto) {
    return this.InventoryService.update(+id, updateInventoryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.InventoryService.remove(+id);
  }
}
