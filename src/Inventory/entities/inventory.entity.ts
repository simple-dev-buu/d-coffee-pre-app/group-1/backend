/* eslint-disable prettier/prettier */
import { Branch } from 'src/branches/entities/branch.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { InventoryItem } from './inventoryItems.entity';

@Entity()
export class Inventory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  totalValue: number;

  @ManyToOne(() => Branch, (branch) => branch.inventory)
  @JoinColumn()
  branch: Branch;

  @OneToMany(() => InventoryItem, (inventoryItem) => inventoryItem.inventory)
  inventoryItem: InventoryItem[];
}
