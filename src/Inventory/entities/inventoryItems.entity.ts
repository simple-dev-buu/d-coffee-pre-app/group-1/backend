import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Ingredient } from 'src/ingredient/entities/ingredient.entity';
import { Inventory } from './inventory.entity';

@Entity()
export class InventoryItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  balance: number;

  @Column()
  minBalance: number;

  @Column()
  value: number;

  @ManyToOne(() => Inventory, (inventory) => inventory.inventoryItem)
  @JoinColumn()
  inventory: Inventory;

  @ManyToOne(() => Ingredient, (ingredient) => ingredient.inventoryItem)
  @JoinColumn()
  ingredient: Ingredient;
}
