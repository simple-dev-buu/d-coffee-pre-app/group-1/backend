import { Injectable } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Bill } from './entities/bill.entity';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private BillRepository: Repository<Bill>,
  ) {}

  create(createBillDto: CreateBillDto) {
    const bill = new Bill();
    bill.name = createBillDto.name;
    bill.worth = createBillDto.worth;
    bill.status = createBillDto.status;
    return this.BillRepository.save(bill);
  }

  findAll() {
    return this.BillRepository.find();
  }

  findOne(id: number) {
    return this.BillRepository.findOneBy({ id: id });
  }

  findByStat(status: boolean) {
    return this.BillRepository.find({
      where: {
        status: status,
      },
    });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    await this.BillRepository.update(id, updateBillDto);
    const bill = await this.BillRepository.findOneBy({ id: id });
    return bill;
  }

  async remove(id: number) {
    const delBill = await this.BillRepository.findOneByOrFail({ id: id });
    await this.BillRepository.remove(delBill);
    return delBill;
  }
}
