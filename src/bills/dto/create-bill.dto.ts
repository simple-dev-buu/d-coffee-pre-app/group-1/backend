export class CreateBillDto {
  name: string;
  worth: number;
  status: boolean;
}
