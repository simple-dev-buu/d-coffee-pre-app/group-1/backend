import { IsNotEmpty, IsNumber, IsString } from '@nestjs/class-validator';

export class CreateEmployeeDto {
  @IsString()
  role: string;

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  tel: string;

  @IsString()
  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';

  @IsString()
  @IsNotEmpty()
  birthDate: string;

  @IsString()
  @IsNotEmpty()
  qualification: string;

  @IsNumber()
  moneyRate: number;

  @IsNumber()
  minWork: number;

  @IsString()
  startDate: string;
}
