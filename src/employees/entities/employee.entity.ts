import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  role: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  tel: string;

  @Column()
  gender: 'male' | 'female' | 'others';

  @Column()
  birthDate: string;

  @Column()
  qualification: string;

  @Column({ default: 0 })
  moneyRate: number;

  @Column({ default: 0 })
  minWork: number;

  @Column()
  startDate: string;
}
